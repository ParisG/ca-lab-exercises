#include <iostream>

using namespace std;

void Swap(int *x, int *y)
{
    int tmp = *x;
    *x = *y;
    *y = tmp;
}

void BubbleSort(int* array)
{
    for (int i=sizeof(array)/sizeof(*array); i>0; i--)
        for (int j=0; j<=i; j++)
            if (*(array+j) > *(array+(j+1)))
                Swap(&array[j], &array[j+1]);
}

int main()
{
    int array[5] = {7, 4, 1, 2, 3};
    for (int i=0; i<5; i++)
        cout << array[i] << " ";
    BubbleSort(array);
    cout << endl;
    for (int i=0; i<5; i++)
        cout << array[i] << " ";
	return 0;
}
