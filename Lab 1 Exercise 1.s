				  # Computer Architecture 2013 Lab Exercise 1.1
	                	  # Christos Sotirelis - 321/2012182
				  # Parisis Ganitis    - 321/2012030
				  
  .data				  # Program data and initialization under the .data directive
name: .asciiz "Ganitis Parisis\n" # String for printing the name and move to the next line
am: .asciiz "321/2012030"         # String for printing the student ID

  .text				  # Program start under the .text directive
main:				  # Standard label for the main program (start)
 la $a0, name   		  # Load the address of name string to $a0 for printing
 li $v0, 4			  # Store the system call value for print_string to $v0
 syscall		 	  # Command to execute the system call (print name)
 
 la $a0, am			  # Load the address of am string to $a0 for printing
 li $v0, 4			  # Store the system call value for print_string to $v0
 syscall		 	  # Command to execute the system call (print am)
 
 li $v0, 10			  # Store the system call value for exit to $v0
 syscall			  # Command to execute the system call and terminate the program
 
 # O PARHS GAMIETAI