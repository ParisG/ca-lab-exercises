							# Computer Architecture 2013 Lab Exercise 5
							# Christos Sotirelis - 321/2012182
							# Parisis Ganitis    - 321/2012030

  .data 					     	# Program data and initialization under the .data directive
numarray: .word 0:10					# Array of 10 integers initialized to zero
inputmsg: .asciiz "Give integer " 			# String to print the input message
inputmsg2: .asciiz ": " 				
unsorted: .asciiz "Unsorted array: " 			# String to print the unsorted array message
sorted: .asciiz "Sorted array: " 			# String to print the sorted array message
space: .asciiz " "					# String to print a space
newline: .asciiz "\n"					# String to print a new line

  .text						     	# Program start under the .text directive
main:						     	# Standard label for the main program (start)
  li $t0, 0						# Array counter
  li $t1, 1						# Loop iterations counter
  li $t2, 5						# TODO: CHANGE VALUE TO 11 - End of loop
  
loop:							# Loop label for data input
  la $a0, inputmsg					# Load the address of inputmsg string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call
  move $a0, $t1						# Move $t1 to $a0 to print it
  li $v0, 1						# Store the system call value for print_int to $v0
  syscall						# Command to execute the system call
  la $a0, inputmsg2					# Load the address of inputmsg2 string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call
  li $v0, 5					     	# Store the system call value for read_int to $v0
  syscall					     	# Command to execute the system call
  sw $v0, numarray + 0($t0)				# Store integer to array
  addi $t0, $t0, 4					# Move to the next array position
  addi $t1, $t1, 1					# Move to the next iteration
  blt $t1, $t2, loop					# Jump to loop until 11 is reached
  
  la $a0, unsorted					# Load the address of unsorted message string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call
  la $a0, newline					# Load the address of newline string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call
  
  li $s0, 0						# Bool register checking wether the array is sorted or not (0 = unsorted, 1 = sorted)
  li $t1, 0						# Start of display loop
  
display:						# Display label
  lw $a0, numarray + 0($t1)				# Load current element from numarray (memory) to $a0 to print it
  li $v0, 1						# Store the system call value for print_int to $v0
  syscall						# Command to execute the system call (print element)
  la $a0, space						# Load the address of space string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call
  addi $t1, $t1, 4					# Move to the next array position
  blt $t1, $t0, display					# Jump to display until all elements are printed ($t0 is the last position of the array)
  
  beq $s0, $zero, sort					# Jump to sort label if the array is unsorted
  bne $s0, $zero, end					# Jump to end label if the array is sorted (already displayed)

sort:							# Sort label
  move $s1, $t0						# Move and hold last position of array
sort1:							# Outer loop of bubblesort
  li $t4, 0						# Will hold current array element
  li $s4, 0						# Will hold next array element
  sort2:						# Inner loop of bubblesort
    add $s4, $t4, 4					# Get next array element
    bgt $t4, $s4, swap					# Jump to swap if next array element is bigger than current
    blt $t4, $s4, next					# Otherwise, jump to next (next iteration)
    swap:						# Swap label (uses temporarily register $s5 to swap values of $s4 and $t4)
      move $s5, $s4
      move $s4, $t4
      move $t4, $s5
    next:						# Next label
    addi $t4, $t4, 1					# Move to the next iteration
    ble $t4, $s1, sort2					# Jump to sort2 while $t4 is less than or equal to $s1
    bgt $t4, $s1, sort1					# Otherwise, jump to sort1
  addi $s1, $s1, -4					# Move to the next array element
bgt $s1, $zero, sort1					# Jump to sort1 until zero is reached (sorted array)

  la $a0, newline					# Load the address of newline string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call
  la $a0, sorted					# Load the address of sorted message string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call
  la $a0, newline					# Load the address of newline string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call

  li $s0, 1						# Change bool register to 1 (array is now sorted)
  li $t1, 0						# Start of display loop
  j display						# Jump to display label and print the sorted array

end:							# End label
  li $v0, 10					     	# Store the system call value for exit to $v0
  syscall					        # Command to execute the system call and terminate the program