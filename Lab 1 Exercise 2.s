						     # Computer Architecture 2013 Lab Exercise 1.2
						     # Christos Sotirelis - 321/2012182
						     # Parisis Ganitis    - 321/2012030

  .data 					     # Program data and initialization under the .data directive
msg1: .asciiz "Input first integer: " 		     # String for printing the first input message
msg2: .asciiz "Input second integer: " 	     	     # String for printing the second input message
resmsg: .asciiz "The result of the subtraction is: " # String for printing the result

  .text						     # Program start under the .text directive
main:						     # Standard label for the main program (start)
 la $a0, msg1					     # Load the address of msg1 string to $a0 for printing
 li $v0, 4					     # Store the system call value for print_string to $v0
 syscall				       	     # Command to execute the system call (print msg1)
 
 li $v0, 5					     # Store the system call value for read_int to $v0
 syscall					     # Command to execute the system call
 move $t0, $v0					     # Move the first integer from $v0 to $t0
 
 la $a0, msg2					     # Load the address of msg2 string to $a0 for printing
 li $v0, 4					     # Store the system call value for print_string to $v0
 syscall					     # Command to execute the system call (print msg2)
 
 li $v0, 5					     # Store the system call value for read_int to $v0
 syscall					     # Command to execute the system call
 move $t1, $v0					     # Move the second integer from $v0 to $t1
 
 sub $t0, $t0, $t1				     # Subtract $t1 from $t0 and store it to $t0						
 
 la $a0, resmsg					     # Load the address of resultmsg string to $a0 for printing
 li $v0, 4					     # Store the system call value for print_string to $v0
 syscall					     # Command to execute the system call (print resmsg)
 
 move $a0, $t0					     # Move the result from $t0 to $a0 for printing
 li $v0, 1					     # Store the system call value for print_int to $v0
 syscall 					     # Command to execute the system call (print result)
 
 li $v0, 10					     # Store the system call value for exit to $v0
 syscall					     # Command to execute the system call and terminate the program
 