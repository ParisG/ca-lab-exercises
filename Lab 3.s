							# Computer Architecture 2013 Lab Exercise 3
							# Christos Sotirelis - 321/2012182
							# Parisis Ganitis    - 321/2012030

  .data 					     	# Program data and initialization under the .data directive
welcome: .asciiz "Calculation of the Greatest Common Divisor of two integers using Euclid's algorithm.\n" # String to print the welcome messsage
inputmsg1: .asciiz "Input the first integer: " 		# String to print the first input message
inputmsg2: .asciiz "Input the second integer: " 	# String to print the second input message
zerosmsg: .asciiz "Both numbers are 0s!!!\n"		# String to print the zeros message
resultmsg: .asciiz "The Greatest Common Divisor is: "	# String to print the result message
newline: .asciiz "\n"					# String to print a new line

  .text						     	# Program start under the .text directive
main:						     	# Standard label for the main program (start)
  la $a0, welcome					# Load the address of welcome string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall				       	        # Command to execute the system call (print welcome message)
  
prompt:							# Prompt label (getting input)
  la $a0, inputmsg1					# Load the address of inputmsg1 string to $a0 to print it
  li $v0, 4					     	# Store the system call value for print_string to $v0
  syscall				       	     	# Command to execute the system call (print inputmsg1)
   
  li $v0, 5					     	# Store the system call value for read_int to $v0
  syscall					     	# Command to execute the system call (get first integer)
  move $t0, $v0					     	# Move first integer from $v0 to $t0
   
  la $a0, inputmsg2					# Load the address of inputmsg2 string to $a0 to print it
  li $v0, 4					     	# Store the system call value for print_string to $v0
  syscall					     	# Command to execute the system call (print inputmsg2)
   
  li $v0, 5					     	# Store the system call value for read_int to $v0
  syscall					     	# Command to execute the system call (get second integer)
  move $t1, $v0					     	# Move second integer from $v0 to $t1
  
  beq $t0, $t1, checkifzero				# If numbers are equal jump to checkifzero label (check if they're zero)
  
  beq $t0, $zero, firstiszero				# If (only) the first number is zero jump to firstiszero label (set second number as the Greatest Common Divisor)
  beq $t1, $zero, result				# If (only) the second number is zero jump directly to result label ($t0 is used for the result anyway)
  
  blt $t0, $zero, negtopos0				# If the first number is negative jump to negtopos0 label (convert it to positive)
next:							# Next label
  blt $t1, $zero, negtopos1				# If the second number is negative jump to negtopos1 label (convert it to positive)
  
gcd:							# Greatest Common Divisor label (calculation)
  div $t0, $t1						# Divide the two numbers
  move $t0, $t1						# Move the second to the first's register (for the next loop/division)
  mfhi $t1						# Move the remainder from Hi register to $t1
  bne $t1, $zero, gcd					# Jump to gcd label until $t1 reaches zero (end of calculation)

result:							# Result label
  la $a0, resultmsg					# Load the address of resultmsg string to $a0 to print it
  li $v0, 4					     	# Store the system call value for print_string to $v0
  syscall						# Command to execute the system call (print resultmsg)
  
  move $a0, $t0						# Move the result from $t0 to $a0 to print it
  li $v0, 1						# Store the system call value for print_int to $v0
  syscall						# Command to execute the system call (print result)
  
  la $a0, newline					# Load the address of newline string to $a0 to print it
  li $v0, 4						# Store the system call value for print_string to $v0
  syscall						# Command to execute the system call (print resultmsg)
  
  j prompt						# Jump to prompt label (get next input without printing welcome message)
  
  li $v0, 10					     	# Store the system call value for exit to $v0
  syscall					        # Command to execute the system call and terminate the program

negtopos0:						# Negative to positive convertion of the first integer
  sub $t0, $zero, $t0					# Subtract it from zero
  j next						# Jump to next label (check if the second integer is negative)

negtopos1:						# Negative to positive convertion of the second integer
  sub $t1, $zero, $t1					# Subtract it from zero
  j gcd							# Jump to gcd label (calculation of GCD)

checkifzero:						# Check if any of two integers is zero (numbers are already equal)
  beq $t0, $zero, zeros					# If the first integer is zero jump to zeros label (desired message)
  bne $t0, $zero, gcd					# If the first integer isn't zero jump to gcd label (calculation of GCD)
  
zeros:							# Zeros label (both numbers are zeros)
  la $a0, zerosmsg					# Load the address of zerosmsg string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall						# Command to execute the system call (print zerosmsg)
  j prompt						# Jump to prompt label (get new inputs)
  
firstiszero:						# The first integer is zero (the second is the GCD)
  move $t0, $t1						# Move the second integer to first's register
  j result						# Jump to result (prints $t0, in that case GCD)