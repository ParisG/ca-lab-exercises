							# Computer Architecture 2013 Lab Exercise 4
							# Christos Sotirelis - 321/2012182
							# Parisis Ganitis    - 321/2012030

  .data 					     	# Program data and initialization under the .data directive
numarray: .word 0:20					# Array of 20 integers initialized to zero
inputmsg1: .asciiz "Input the first term of sequence: " # String to print the first input message
inputmsg2: .asciiz "Input the common difference: " 	# String to print the second input message
summsg: .asciiz "The sum of the 20 first terms of the sequence is: " # String to print the sum message
sum: .word 0						# Variable of sum
space: .asciiz " "					# String to print a space
newline: .asciiz "\n"					# String to print a new line

  .text						     	# Program start under the .text directive
main:						     	# Standard label for the main program (start)
  la $a0, inputmsg1					# Load the address of inputmsg1 string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall				       	        # Command to execute the system call (print inputmsg1)
   
  li $v0, 5					     	# Store the system call value for read_int to $v0
  syscall					     	# Command to execute the system call (get first integer/term of sequence)
  move $t0, $v0					     	# Move first integer from $v0 to $t0
   
  la $a0, inputmsg2					# Load the address of inputmsg2 string to $a0 to print it
  li $v0, 4					     	# Store the system call value for print_string to $v0
  syscall					     	# Command to execute the system call (print inputmsg2)
   
  li $v0, 5					     	# Store the system call value for read_int to $v0
  syscall					     	# Command to execute the system call (get common difference of arithmetic progression)
  move $t1, $v0					     	# Move second integer from $v0 to $t1
 
  li $t5, 1						# Loop counter
  li $t6, 20						# Loop end
  li $t3, 0						# Array index
  li $t7, 0						# Sum variable
  
  sw $t0, numarray + 0($t3)				# Store first term of sequence to first array position
  
loop:							# Loop label (calculate and insert values to array)
  lw $t0, numarray + 0($t3)				# Load previous array element from memory to $t0 register
  add $t4, $t0, $t1					# Add common difference to current element and store it at $t4
  addi $t3, $t3, 4					# Move array index to next element (plus 4)
  sw $t4, numarray + 0($t3)				# Store $t4 (result) value to current array position
  add $t7, $t7, $t4					# Add $t4 (result) to sum register ($t7)
  addi $t5, $t5, 1					# Move loop counter to next loop
  blt $t5, $t6, loop					# Jump to loop label until 20 is reached
 
display:						# Display label (print array reversed)
  lw $t2, numarray + 0($t3)				# Load current element from numarray (memory) to $t2 register
  move $a0, $t2						# Move $t2 to $a0 to print it
  li $v0, 1						# Store the system call value for print_int to $v0
  syscall						# Command to execute the system call (print element)
  la $a0, space						# Load the address of summsg string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall				       	        # Command to execute the system call (print sum)
  addi $t3, $t3, -4					# Move array index to previous element (minus 4)
  bge $t3, $zero, display				# Jump to display label until index reaches zero (first element)
  
  la $a0, newline					# Load the address of newline string to $a0 to print it
  li $v0, 4						# Store the system call value for print_string to $v0
  syscall						# Command to execute the system call (print resultmsg)
  
  la $a0, summsg					# Load the address of summsg string to $a0 to print it
  li $v0, 4					        # Store the system call value for print_string to $v0
  syscall				       	        # Command to execute the system call (print summsg)
  
  move $a0, $t7						# Move sum from $t7 to $a0 to print it
  li $v0, 1						# Store the system call value for print_int to $v0
  syscall						# Command to execute the system call (print sum)
  
  sw $t7, sum						# Store value of $t7 (sum) to memory (sum variable)
  
  li $v0, 10					     	# Store the system call value for exit to $v0
  syscall					        # Command to execute the system call and terminate the program