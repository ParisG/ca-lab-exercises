									     # Computer Architecture 2013 Lab Exercise 2
									     # Christos Sotirelis - 321/2012182
									     # Parisis Ganitis    - 321/2012030

  .data 					     			     # Program data and initialization under the .data directive
inputmsg: .asciiz "Input the amount of numbers for the Fibonacci sequence: " # String for printing the input message
space: .asciiz " "                                                           # String for printing the space character

  .text						     			     # Program start under the .text directive
main:						     			     # Standard label for the main program (start)
  la $a0, inputmsg					     		     # Load the address of inputmsg string to $a0 for printing
  li $v0, 4					                             # Store the system call value for print_string to $v0
  syscall				       	         		     # Command to execute the system call (print inputmsg)
  
  li $v0, 5					     			     # Store the system call value for read_int to $v0
  syscall					      			     # Command to execute the system call
  move $s0, $v0					     			     # Move the first integer from $v0 to $s0
  
  li $a0, 0								     # Load 0 to $a0 to print the first number (0)
  li $v0, 1								     # Store the system call value for print_int to $v0
  syscall								     # Command to execute the system call (print 0)
  
  la $a0, space 							     # Load the address of space string to $a0 for printing
  li $v0, 4					                             # Store the system call value for print_string to $v0
  syscall				       	         		     # Command to execute the system call (print space)
  
  li $a0, 1								     # Load 1 to $a0 to print the second number (1)
  li $v0, 1								     # Store the system call value for print_int to $v0
  syscall								     # Command to execute the system call (print 1)
  
  li $s1, 2								     # Load 2 to $s1 (start index of loop)
  li $s2, 0								     # Load 0 to $s2 (first number of fibonacci sequence)
  li $s3, 1								     # Load 1 to $s3 (second number of fibonacci sequence)
  
loop:									     # Start of fibonacci loop (label)
  la $a0, space 							     # Load the address of space string to $a0 for printing
  li $v0, 4					                             # Store the system call value for print_string to $v0
  syscall				       	         		     # Command to execute the system call (print space)
  
  add $s4, $s2, $s3							     # Add the two previous numbers and move the result to $s4
  
  move $a0, $s4                                                              # Move the result to $a0 for printing
  li $v0, 1								     # Store the system call value for print_int to $v0
  syscall								     # Command to execute the system call (print result)
  
  move $s2, $s3								     # Move $s3 to $s2 to prepare them for the next loop
  move $s3, $s4                                                              # Move $s4 to $s3 to prepare them for the next loop
  addi $s1, $s1, 1							     # Increase $s1 by 1 to get to the next loop
  
  blt $s1, $s0, loop        						     # Go to loop label if $s1 < $s0 (until the input number is reached)
  									     # Otherwise, move on
  
  li $v0, 10					     			     # Store the system call value for exit to $v0
  syscall					                             # Command to execute the system call and terminate the program
  